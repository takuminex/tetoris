document.getElementById("hello_text").textContent = "初めてのJavaScript";

var count = 0;

var cells;//ã²ã¼ã ç¤ãç¤ºãå¤æ°

// ãã­ãã¯ã®ãã¿ã¼ã³
var blocks = {
  i: {
    class: "i",
    pattern: [
      [1, 1, 1, 1]
    ]
  },
  o: {
    class: "o",
    pattern: [
      [1, 1], 
      [1, 1]
    ]
  },
  t: {
    class: "t",
    pattern: [
      [0, 1, 0], 
      [1, 1, 1]
    ]
  },
  s: {
    class: "s",
    pattern: [
      [0, 1, 1], 
      [1, 1, 0]
    ]
  },
  z: {
    class: "z",
    pattern: [
      [1, 1, 0], 
      [0, 1, 1]
    ]
  },
  j: {
    class: "j",
    pattern: [
      [1, 0, 0], 
      [1, 1, 1]
    ]
  },
  l: {
    class: "l",
    pattern: [
      [0, 0, 1], 
      [1, 1, 1]
    ]
  }
};

loadTable();//ã²ã¼ã ç¤ãèª­ã¿è¾¼ã

setInterval(function() {
    count++;
    document.getElementById("hello_text").textContent = "ã¯ããã¦ã®JavaScript(" + count + ")";

    if (hasFallingBlock()) { // è½ä¸ä¸­ã®ãã­ãã¯ããããç¢ºèªãã
	fallBlocks();// ããã°ãã­ãã¯ãè½ã¨ã
    } else { // ãªããã°
	//deleteRow();// ããã£ã¦ããè¡ãæ¶ã
	generateBlock();// ã©ã³ãã ã«ãã­ãã¯ãä½æãã
    }
}, 1000);

function loadTable(){
    //ã²ã¼ã ç¤ãå¤æ°cellsã«ã¾ã¨ãã
    cells = [];
    var td_array = document.getElementsByTagName("td");
    var index = 0;
    for (var row = 0; row < 20; row++) {
	cells[row] = [];
	for (var col = 0; col < 10; col++) {
	    cells[row][col] = td_array[index];
	    index++;
	}
    }
}


/*function fallBlocks() {
  // ä¸çªä¸ã®è¡ã«ãã­ãã¯ãããã°è½ä¸ä¸­ã®ãã©ã°ãfalseã«ãã
  for (var i = 0; i < 10; i++) {
    if (cells[19][i].className !== "") {
      isFalling = false;
      return; // ä¸çªä¸ã®è¡ã«ãã­ãã¯ãããã®ã§è½ã¨ããªã
    }
  }
  // ä¸ããäºçªç®ã®è¡ããç¹°ãè¿ãã¯ã©ã¹ãä¸ãã¦ãã
  for (var row = 18; row >= 0; row--) {
    for (var col = 0; col < 10; col++) {
      if (cells[row][col].className !== "") {
        cells[row + 1][col].className = cells[row][col].className;
        cells[row][col].className = "";
      }
    }
  }
}


var isFalling = false;
function hasFallingBlock() {
  // è½ä¸ä¸­ã®ãã­ãã¯ããããç¢ºèªãã
  return isFalling;
}

function generateBlock() {
    // ã©ã³ãã ã«ãã­ãã¯ãçæãã
    // 1. ãã­ãã¯ãã¿ã¼ã³ããã©ã³ãã ã«ä¸ã¤ãã¿ã¼ã³ãé¸ã¶
    var keys = Object.keys(blocks);
    var nextBlockKey = keys[Math.floor(Math.random() * keys.length)];
    var nextBlock = blocks[nextBlockKey];
    // 2. é¸ãã ãã¿ã¼ã³ããã¨ã«ãã­ãã¯ãéç½®ãã
    var pattern = nextBlock.pattern;
    for (var row = 0; row < pattern.length; row++) {
	for (var col = 0; col < pattern[row].length; col++) {
	    if (pattern[row][col]) {
            cells[row][col + 3].className = nextBlock.class;
	    }
	}
    }
    // 3. è½ä¸ä¸­ã®ãã­ãã¯ãããã¨ãã
    isFalling = true;
}
*/

function fallBlocks() {
  // 1. åºã«ã¤ãã¦ããªããï¼
  for (var col = 0; col < 10; col++) {
    if (cells[19][col].blockNum === fallingBlockNum) {
      isFalling = false;
      return; // ä¸çªä¸ã®è¡ã«ãã­ãã¯ãããã®ã§è½ã¨ããªã
    }
  }
  // 2. 1ãã¹ä¸ã«å¥ã®ãã­ãã¯ããªããï¼
  for (var row = 18; row >= 0; row--) {
    for (var col = 0; col < 10; col++) {
      if (cells[row][col].blockNum === fallingBlockNum) {
        if (cells[row + 1][col].className !== "" && cells[row + 1][col].blockNum !== fallingBlockNum){
          isFalling = false;
          return; // ä¸ã¤ä¸ã®ãã¹ã«ãã­ãã¯ãããã®ã§è½ã¨ããªã
        }
      }
    }
  }
  // ä¸ããäºçªç®ã®è¡ããç¹°ãè¿ãã¯ã©ã¹ãä¸ãã¦ãã
  for (var row = 18; row >= 0; row--) {
    for (var col = 0; col < 10; col++) {
      if (cells[row][col].blockNum === fallingBlockNum) {
        cells[row + 1][col].className = cells[row][col].className;
        cells[row + 1][col].blockNum = cells[row][col].blockNum;
        cells[row][col].className = "";
        cells[row][col].blockNum = null;
      }
    }
  }
}


var isFalling = false;
function hasFallingBlock() {
  // è½ä¸ä¸­ã®ãã­ãã¯ããããç¢ºèªãã
  return isFalling;
}

var fallingBlockNum = 0;
function generateBlock() {
  // ã©ã³ãã ã«ãã­ãã¯ãçæãã
  // 1. ãã­ãã¯ãã¿ã¼ã³ããã©ã³ãã ã«ä¸ã¤ãã¿ã¼ã³ãé¸ã¶
  var keys = Object.keys(blocks);
  var nextBlockKey = keys[Math.floor(Math.random() * keys.length)];
  var nextBlock = blocks[nextBlockKey];
  var nextFallingBlockNum = fallingBlockNum + 1;
  // 2. é¸ãã ãã¿ã¼ã³ããã¨ã«ãã­ãã¯ãéç½®ãã
  var pattern = nextBlock.pattern;
  for (var row = 0; row < pattern.length; row++) {
    for (var col = 0; col < pattern[row].length; col++) {
      if (pattern[row][col]) {
        cells[row][col + 3].className = nextBlock.class;
        cells[row][col + 3].blockNum = nextFallingBlockNum;
      }
    }
  }
  // 3. è½ä¸ä¸­ã®ãã­ãã¯ãããã¨ãã
  isFalling = true;
  fallingBlockNum = nextFallingBlockNum;
}


